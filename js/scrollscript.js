      
 function moveScroller() {
    var move = function() {
        var st = $(window).scrollTop();
        var ot = $("#sticky-anchor").offset().top;
        var s = $("#sc-menu-floating");
        if(st > ot) {
            s.css({
                 position: fixed;
                 top: 0px;
                
            });
        } else {
            if(st <= ot) {
                s.css({
                    position: absolute;
                    top: 113px;
                    left:-120px;
                    z-index: 1100;
                });
            }
        }
    };
    $(window).scroll(move);
    move();
}
 