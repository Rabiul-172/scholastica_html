/* Top Menu Generation Script
* Author: Md. Iqbal Baksh
* Last updated: 20140203
*/ 

function createTopMenu(){
  var td = document.getElementById('topMenuHolder');
  if (!td) return; // silently return, if the menu holder is not found in the DOM
  var s = '<div id="cssmenu">';
  s += '<ul>';
  s += '<li class="active"><a href="index.html"><span>Home</span></a></li>';
  s += '<li class="has-sub"><a href="academics.html"><span>Academics</span></a>';
  s += '  <ul>';
  s += '    <li><a href="curriculum.html"><span>Our Curriculum</span></a></li>';
  s += '    <li><a href="school-calendar.html"><span>School Year & Academic Calendar</span></a></li>';
  s += '    <li class="last"><a href="academic-supports.html"><span>Academic Services &amp; Supports</span></a></li>';
  s += '  </ul>';
  s += '</li>';
  s += '<li class="has-sub"><a href="admission.html"><span>Admission</span></a>';
  s += '  <ul>';
  s += '    <li><a href="how-to-apply.html"><span>How to Apply</span></a></li>';
  s += '    <li class="last"><a href="fin-aid.html"><span>Financial Assistance</span></a></li>';
  s += '    <li class="last"><a href="fees.html"><span>Fees</span></a></li>';
  s += '    <li class="last"><a href="faq.html"><span>FAQ (Frequently Asked Questions)</span></a></li>';
  s += '  </ul>';
  s += '</li>';
  s += '<li class="has-sub"><a href="parent-student.html"><span>Parents &amp; Students</span></a>';
  s += '  <ul>';
  s += '    <li><a href="parents.html"><span>Parents in the School Community</span></a></li>';
  s += '    <li class="last"><a href="students.html"><span>Student Life</span></a></li>';
  s += '    <li class="last"><a href="facilities.html"><span>Facilities</span></a></li>';
  s += '    <li class="last"><a href="services.html"><span>Services for Students</span></a></li>';
  s += '  </ul>';
  s += '</li>';
  s += '<li class="has-sub"><a href="about-us.html"><span>About Us</span></a>';
  s += '  <ul>';
  s += '    <li class="last"><a href="ascent-group.html"><span>Ascent Group</span></a></li>';
  s += '    <li class="last"><a href="alumni.html"><span>Scholastica Alumni Association</span></a></li>';
  s += '    <li class="last"><a href="jobs.html"><span>Jobs @ Scholastica</span></a></li>';
  s += '  </ul>';
  s += '</li>';
  s += '<li class="last"><a href="contact.html"><span>Contact Us</span></a></li>';
  s += '</ul>';
  s += '</div>';
  td.innerHTML = s;
  delete s;
  return;
};